# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-21 00:36+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .\sitetemplates\base_root.html.py:76
#, python-format
msgid "You are logged in as %(user)s"
msgstr "Vous êtes connecté(e) en tant que %(user)s"

#: .\sitetemplates\base_root.html.py:77
msgid "Edit Profile"
msgstr "Modifier profil"

#: .\sitetemplates\base_root.html.py:78
msgid "Logout"
msgstr "Déconnexion"

#: .\sitetemplates\base_root.html.py:80
msgid "You are not logged in."
msgstr "Vous n'êtes pas connecté(e)"

#: .\sitetemplates\base_root.html.py:81
msgid "Login"
msgstr "Connexion"

#: .\sitetemplates\base_root.html.py:82
msgid "Register"
msgstr "S'enregistrer"

#: .\sitetemplates\base_root.html.py:89 .\sitetemplates\base_root.html.py:93
msgid "Change Language"
msgstr "Changer le langage"

#: .\sitetemplates\base_root.html.py:105
msgid "Manage Roles"
msgstr "Gérer les rôles"

#: .\sitetemplates\base_root.html.py:106
msgid "Manage Role Groups"
msgstr "Gérer les groupes"
